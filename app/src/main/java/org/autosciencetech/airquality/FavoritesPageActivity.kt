package org.autosciencetech.airquality

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_favorites_page.*
import kotlinx.android.synthetic.main.content_favorites_page.*

class FavoritesPageActivity : AppCompatActivity() {
    val PREFS_FILENAME = "org.autosciencetech.utahair"
    lateinit var prefs: SharedPreferences
    private lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private lateinit var adapter: RecyclerAdapter
    private lateinit var stationStringArrayList: ArrayList<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites_page)
        setSupportActionBar(toolbar)
        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        stationStringArrayList = intent.getStringArrayListExtra("stationList") as ArrayList<String>

        prefs = this.getSharedPreferences(PREFS_FILENAME, 0)

        adapter = RecyclerAdapter(stationStringArrayList, prefs)
        recyclerView.adapter = adapter

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.save, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.save_button -> {
                val intent = Intent(this, MainActivity::class.java)
                navigateUpTo(intent)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        navigateUpTo(intent)
    }
}

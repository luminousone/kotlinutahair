package org.autosciencetech.airquality

import android.Manifest
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.google.android.gms.wearable.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.HttpsURLConnection
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    val PREFS_FILENAME = "org.autosciencetech.utahair"
    private lateinit var prefs: SharedPreferences
    private lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private lateinit var adapter: FavoritesAdapter
    private lateinit var stationStringArrayList: ArrayList<String?>
    private lateinit var favoritesRecyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var mainContentLayout: ConstraintLayout
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var currentStation: Int = 0
    private var previousStation: Int = -1
    private var nextStation: Int = 1
    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        prefs = this.getSharedPreferences(PREFS_FILENAME, 0)
        favoritesRecyclerView = findViewById(R.id.favoritesRecyclerView)
        mainContentLayout = findViewById(R.id.mainConstraintLayout)
        mainContentLayout.setOnTouchListener(object:OnSwipeTouchListener(this@MainActivity){
            override fun onSwipeUp() {
                super.onSwipeUp()
                if(currentStation >= stationStringArrayList.size-1){
                    refresh()
                    nextStation = stationStringArrayList.size
                    previousStation = stationStringArrayList.size - 2
                    currentStation = stationStringArrayList.size- 1
                    Toast.makeText(this@MainActivity, "refreshing", Toast.LENGTH_SHORT)
                        .show()
                }else{
                    stationStringArrayList[nextStation]?.let { updateStation(it) }
                    nextStation++
                    previousStation++
                    currentStation = nextStation -1
                }
            }

            override fun onSwipeDown() {
                super.onSwipeDown()
                if(currentStation <= 0){
                    refresh()
                    previousStation = -1
                    currentStation = 0
                    nextStation = 1
                    Toast.makeText(this@MainActivity, "refreshing", Toast.LENGTH_SHORT)
                        .show()
                }else{
                    stationStringArrayList[previousStation]?.let { updateStation(it) }
                    previousStation--
                    nextStation--
                    currentStation = nextStation -1
                }
            }
        })

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        favoritesRecyclerView.layoutManager = linearLayoutManager
        stationStringArrayList = ArrayList<String?>()

        for (site: String in prefs.getStringSet("favorites", HashSet<String>()).orEmpty()){
            stationStringArrayList.add(prefs.getString(site,""))
        }

        stationStringArrayList.sortWith(compareBy({JSONObject(it).getJSONObject("station").getString("region")},{JSONObject(it).getJSONObject("station").getString("label")},{JSONObject(it).getJSONObject("station").getString("name")}))

        if (prefs.contains("curLoc") && prefs.getString("curLoc","") != "")
            stationStringArrayList.add(0, prefs.getString("curLoc",""))

        if (prefs.contains("defaultLoc") && prefs.getString("defaultLoc","curLoc") != "curLoc"){
            val locationOfDefault = stationStringArrayList.indexOf(prefs.getString(prefs.getString("defaultLoc","curLoc"),""))
            if (locationOfDefault>0){
                val defaultStation = stationStringArrayList[locationOfDefault]
                stationStringArrayList.remove(defaultStation)
                stationStringArrayList.add(0, defaultStation)
            }
        }

        adapter = FavoritesAdapter(stationStringArrayList, this)
        favoritesRecyclerView.adapter = adapter


        if (stationStringArrayList.size > 0)
            updateStation(stationStringArrayList[0]!!)
        //GetData(currentStation,prefs,stationStringArrayList,stationStringArrayList.size).execute()


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val refreshCode = object: Runnable{
            override fun run() {
                if (isConnectedToNetwork())
                    refresh()
                Handler().postDelayed(this,3600000)
            }
        }

        refreshCode.run()

        val jobScheduler = applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val componentName = ComponentName(this, UpdateService::class.java)
        val jobInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            JobInfo.Builder(1337, componentName)
                .setPeriodic(TimeUnit.MINUTES.toMillis(30), TimeUnit.MINUTES.toMillis(15))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setRequiresBatteryNotLow(true)
                .setBackoffCriteria(10000, JobInfo.BACKOFF_POLICY_LINEAR).setPersisted(true).build()
        }else {
            JobInfo.Builder(1337, componentName)
                .setPeriodic(TimeUnit.MINUTES.toMillis(30))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setBackoffCriteria(10000, JobInfo.BACKOFF_POLICY_LINEAR).setPersisted(true).build()
        }
            jobScheduler.schedule(jobInfo)

        if (stationStringArrayList.size > 0 && stationStringArrayList[0] != null) {

            val dataClient: DataClient = Wearable.getDataClient(applicationContext)
            //Toast.makeText(context,"Created Data Client",Toast.LENGTH_LONG).show()

            val putDataReq: PutDataRequest = PutDataMapRequest.create("/station").run {
                dataMap.putString("org.autosciencetech.utahair.station",stationStringArrayList[0])
                asPutDataRequest()
            }
            putDataReq.setUrgent()
            val putDataTask: Task<DataItem> = dataClient.putDataItem(putDataReq)


            putDataTask.addOnSuccessListener {  }
            putDataTask.addOnFailureListener {  }

        }
    }

    override fun onResume() {
        super.onResume()
        if (stationStringArrayList.size > 0 &&stationStringArrayList[0] != null) {

            val dataClient: DataClient = Wearable.getDataClient(applicationContext)
            //Toast.makeText(context,"Created Data Client",Toast.LENGTH_LONG).show()

            val putDataReq: PutDataRequest = PutDataMapRequest.create("/station").run {
                dataMap.putString("org.autosciencetech.utahair.station",stationStringArrayList[0])
                asPutDataRequest()
            }
            putDataReq.setUrgent()
            val putDataTask: Task<DataItem> = dataClient.putDataItem(putDataReq)


            putDataTask.addOnSuccessListener {  }
            putDataTask.addOnFailureListener {  }

        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            finishAffinity()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu
        //val menuItem: MenuItem = menu.findItem(R.id.action_polutant_select)

        /*if (prefs.getString("pollutant","pm25") == "pm25"){
            val editor = prefs.edit()
            editor.putString("pollutant","ozone")
            editor.apply()
            menuItem.setTitle(R.string.menu_ozone)
        }else{
            val editor = prefs.edit()
            editor.putString("pollutant","pm25")
            editor.apply()
            menuItem.setTitle(R.string.menu_pm)
        }*/

        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun showMessage(){

    }

    fun updateStation(stationInput: String){
        currentStation = stationStringArrayList.indexOf(stationInput)
        val station = JSONObject(stationInput)



        val pollutantValueTextView: TextView = findViewById(R.id.pollutantValueTextView)
        val pollutantRangeTextView: TextView = findViewById(R.id.pollutantRangeTextView)
        val messageImageButton: ImageButton = findViewById(R.id.messageImageButton)
        //val messageTextView: TextView = findViewById(R.id.messageTextView)
        val includedForecast: View = findViewById(R.id.includeForecastView)
        val temperatureTextView: TextView = findViewById(R.id.temperatureTextView)
        val windTextView: TextView = findViewById(R.id.windTextView)
        val arrowImageView: ImageView = findViewById(R.id.arrowImageView)
        val updatedTextView: TextView = findViewById(R.id.lastUpdatedTextView)
        val pollutantTypeTextView: TextView = findViewById(R.id.pollutantTypeTextView)
        val stationName: TextView = findViewById(R.id.locationTextView)
        val currentTextView: TextView = findViewById(R.id.currentTextView)

        stationName.text = station.getJSONObject("station").getString("label") + " - " + station.getJSONObject("station").getString("name")
        currentTextView.text = station.getJSONObject("station").getString("name")

        messageImageButton.setOnClickListener {
            val intent = Intent(this, PopUpWindow::class.java)
            intent.putExtra("Message", station.getJSONArray("forecast").getJSONObject(0).getString("message"))
            startActivity(intent)
        }

        //show/hide views depending on available data
        when {
            station.getJSONArray("forecast").length() < 1 -> {
                includedForecast.visibility = View.GONE
                messageImageButton.visibility = View.GONE
                //messageTextView.visibility = View.GONE
            }
            station.getJSONArray("forecast").getJSONObject(0).getString("message") == "" -> {
                //messageTextView.visibility = View.GONE
                messageImageButton.visibility = View.GONE
                includedForecast.visibility = View.VISIBLE
            }
            else -> {
                //messageTextView.visibility = View.VISIBLE
                messageImageButton.visibility = View.VISIBLE
                includedForecast.visibility = View.VISIBLE
                //messageTextView.text = station.getJSONArray("forecast").getJSONObject(0).getString("message")
                //messageTextView.movementMethod = ScrollingMovementMethod()
            }
        }

        //handle pollutant values
        val pollutantType = prefs.getString("pollutant", station.getString("seasonalParameter"))

        for (i in 0 until station.getJSONObject("parameters").getJSONArray("pollutants").length()){
            if(station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("type") == pollutantType){
                pollutantTypeTextView.text = pollutantType!!.capitalize()
                val pollutantValue = station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("value") + station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("unit")
                pollutantValueTextView.text = pollutantValue

                pollutantValueTextView.setTextColor(Color.parseColor("#"+station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("colorHex")))
                pollutantRangeTextView.text = station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("range")
                pollutantRangeTextView.setTextColor(Color.parseColor("#"+station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("colorHex")))
                when(station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("arrow")){
                    "up_red" -> arrowImageView.setImageResource(R.drawable.trendarrow_up_red)
                    "down_green" -> arrowImageView.setImageResource(R.drawable.trendarrow_down_green)
                    else -> arrowImageView.setImageResource(R.drawable.trendarrow_unchange_gray)
                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            updatedTextView.text = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a"))
        else
            updatedTextView.text = SimpleDateFormat("MM/dd/yyyy hh:mm a").format(Date())

        //handle wind and temperature values
        val useMetric = prefs.getBoolean("UseMetric",false)
        val temperature : String
        val wind : String
        if(!useMetric){
            temperature = try{
                station.getJSONObject("parameters").getInt("temperature").toString() + "\u00B0 F"
            }catch (exception: Exception){
                "NA° F"
            }
            wind = try{
                station.getJSONObject("parameters").getDouble("wind_speed").toString() + " mph " + station.getJSONObject("parameters").getString("wind_dir")
            }catch (exception: Exception){
                "NA"
            }
        }else{
            val temp: Double = try{
                (station.getJSONObject("parameters").getDouble("temperature")-32.0)*5.0/9.0
            }catch (exception: Exception){
                0.0
            }
            temperature = "%.2f\u00B0 C".format(temp)
            val windSpeed = try {
                station.getJSONObject("parameters").getDouble("wind_speed") * 1.609344
            }catch (exception: Exception){
                0.0
            }
            wind ="%.2f kph ".format(windSpeed) + station.getJSONObject("parameters").getString("wind_dir")
        }
        temperatureTextView.text = temperature
        windTextView.text = wind

        //handle forecast data
        if(station.getJSONArray("forecast").length() > 0){
            val dayOneTextView: TextView = findViewById(R.id.dayOneTextView)
            val dayOneForecastTextView: TextView = findViewById(R.id.dayOneForcastTextView)
            val dayOneActionImageView: ImageView = findViewById(R.id.dayOneActionImageView)
            val dayOneActionTextView: TextView = findViewById(R.id.dayOneActionTextView)
            val dayTwoTextView: TextView = findViewById(R.id.dayTwoTextView)
            val dayTwoForecastTextView: TextView = findViewById(R.id.dayTwoForcastTextView)
            val dayTwoActionImageView: ImageView = findViewById(R.id.dayTwoActionImageView)
            val dayTwoActionTextView: TextView = findViewById(R.id.dayTwoActionTextView)
            val dayThreeTextView: TextView = findViewById(R.id.dayThreeTextView)
            val dayThreeForecastTextView: TextView = findViewById(R.id.dayThreeForecastTextView)
            val dayThreeActionImageView: ImageView = findViewById(R.id.dayThreeActionImageView)
            val dayThreeActionTextView: TextView = findViewById(R.id.dayThreeActionTextView)

            dayOneTextView.text = station.getJSONArray("forecast").getJSONObject(0).getString("dayOfWeek")
            dayTwoTextView.text = station.getJSONArray("forecast").getJSONObject(1).getString("dayOfWeek")
            dayThreeTextView.text = station.getJSONArray("forecast").getJSONObject(2).getString("dayOfWeek")

            dayOneForecastTextView.text = station.getJSONArray("forecast").getJSONObject(0).getString("severity")
            when(station.getJSONArray("forecast").getJSONObject(0).getString("severity")) {
                "Good" -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_green)
                    dayOneForecastTextView.setTextColor(Color.BLACK)
                }
                "Moderate" -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_yellow)
                    dayOneForecastTextView.setTextColor(Color.BLACK)
                }
                "Unhealthy for Sensitive Groups" -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_orange)
                    dayOneForecastTextView.setTextColor(Color.WHITE)
                }
                "Unhealthy" -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_red)
                    dayOneForecastTextView.setTextColor(Color.WHITE)
                }
                "Very Unhealthy" -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_purple)
                    dayOneForecastTextView.setTextColor(Color.WHITE)
                }
                else -> {
                    dayOneForecastTextView.setBackgroundResource(R.drawable.background_layer_maroon)
                    dayOneForecastTextView.setTextColor(Color.WHITE)
                }
            }
            dayTwoForecastTextView.text = station.getJSONArray("forecast").getJSONObject(1).getString("severity")
            when(station.getJSONArray("forecast").getJSONObject(1).getString("severity")) {
                "Good" -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_green)
                    dayTwoForecastTextView.setTextColor(Color.BLACK)
                }
                "Moderate" -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_yellow)
                    dayTwoForecastTextView.setTextColor(Color.BLACK)
                }
                "Unhealthy for Sensitive Groups" -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_orange)
                    dayTwoForecastTextView.setTextColor(Color.WHITE)
                }
                "Unhealthy" -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_red)
                    dayTwoForecastTextView.setTextColor(Color.WHITE)
                }
                "Very Unhealthy" -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_purple)
                    dayTwoForecastTextView.setTextColor(Color.WHITE)
                }
                else -> {
                    dayTwoForecastTextView.setBackgroundResource(R.drawable.background_layer_maroon)
                    dayTwoForecastTextView.setTextColor(Color.WHITE)
                }
            }
            dayThreeForecastTextView.text = station.getJSONArray("forecast").getJSONObject(2).getString("severity")
            when(station.getJSONArray("forecast").getJSONObject(2).getString("severity")) {
                "Good" -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_green)
                    dayThreeForecastTextView.setTextColor(Color.BLACK)
                }
                "Moderate" -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_yellow)
                    dayThreeForecastTextView.setTextColor(Color.BLACK)
                }
                "Unhealthy for Sensitive Groups" -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_orange)
                    dayThreeForecastTextView.setTextColor(Color.WHITE)
                }
                "Unhealthy" -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_red)
                    dayThreeForecastTextView.setTextColor(Color.WHITE)
                }
                "Very Unhealthy" -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_purple)
                    dayThreeForecastTextView.setTextColor(Color.WHITE)
                }
                else -> {
                    dayThreeForecastTextView.setBackgroundResource(R.drawable.background_layer_maroon)
                    dayThreeForecastTextView.setTextColor(Color.WHITE)
                }
            }

            dayOneActionTextView.text = station.getJSONArray("forecast").getJSONObject(0).getString("action")
            when(station.getJSONArray("forecast").getJSONObject(0).getString("action")){
                "Unrestricted" -> dayOneActionImageView.setImageResource(R.drawable.unrestricted)
                "Voluntary" -> dayOneActionImageView.setImageResource(R.drawable.voluntary)
                else -> dayOneActionImageView.setImageResource(R.drawable.mandatory)
            }
            dayTwoActionTextView.text = station.getJSONArray("forecast").getJSONObject(1).getString("action")
            when(station.getJSONArray("forecast").getJSONObject(1).getString("action")){
                "Unrestricted" -> dayTwoActionImageView.setImageResource(R.drawable.unrestricted)
                "Voluntary" -> dayTwoActionImageView.setImageResource(R.drawable.voluntary)
                else -> dayTwoActionImageView.setImageResource(R.drawable.mandatory)
            }
            dayThreeActionTextView.text = station.getJSONArray("forecast").getJSONObject(2).getString("action")
            when(station.getJSONArray("forecast").getJSONObject(2).getString("action")){
                "Unrestricted" -> dayThreeActionImageView.setImageResource(R.drawable.unrestricted)
                "Voluntary" -> dayThreeActionImageView.setImageResource(R.drawable.voluntary)
                else -> dayThreeActionImageView.setImageResource(R.drawable.mandatory)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_favorites -> {
                val intent = Intent(this, FavoritesLoadingActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_trend_charts -> {
                val intent = Intent(this, TrendChartsActivity::class.java)
                try{
                    intent.putExtra("websiteID",JSONObject(stationStringArrayList[currentStation]).getJSONObject("station").getString("websiteId"))
                    startActivity(intent)
                    return true
                }catch (e: Exception){
                    return false
                }
            }
            R.id.action_legend -> {
                val intent = Intent(this, LegendActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_refresh ->{

                if(isConnectedToNetwork())
                    refresh()

                return true
            }
            R.id.action_polutant_select ->{
                val menuItem: MenuItem = menu.findItem(R.id.action_polutant_select)
                if (prefs.getString("pollutant","") == "pm25"){
                    val editor = prefs.edit()
                    editor.putString("pollutant","ozone")
                    editor.apply()
                    menuItem.setTitle(R.string.menu_ozone)
                }else{
                    val editor = prefs.edit()
                    editor.putString("pollutant","pm25")
                    editor.apply()
                    menuItem.setTitle(R.string.menu_pm)
                }
                try{
                    updateStation(stationStringArrayList[currentStation]!!)
                    return true
                }catch (e: Exception){
                    return false
                }
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun refresh(){
        if(checkPermissions()) {
            var latitude: Double
            var longitude: Double
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationClient!!.lastLocation.addOnCompleteListener { task -> if (task.isSuccessful && task.result != null){
                val lastLocation = task.result
                latitude = lastLocation!!.latitude
                longitude = lastLocation.longitude
                GetLocationData(
                    latitude,
                    longitude,
                    prefs,
                    stationStringArrayList,
                    0
                )
            } else{
                if (stationStringArrayList.size > 0)
                    GetData(stationStringArrayList[currentStation], prefs, stationStringArrayList,0).execute()
            }
            }
        }



        stationStringArrayList.clear()

        for (site in prefs.getStringSet("favorites", HashSet<String>()).orEmpty()){
            stationStringArrayList.add(prefs.getString(site,null))
        }

        stationStringArrayList.sortWith(compareBy({JSONObject(it).getJSONObject("station").getString("region")},{JSONObject(it).getJSONObject("station").getString("label")},{JSONObject(it).getJSONObject("station").getString("name")}))

        if (prefs.contains("curLoc") && prefs.getString("curLoc","") != "")
            stationStringArrayList.add(0, prefs.getString("curLoc",""))

        if (prefs.contains("defaultLoc") && prefs.getString("defaultLoc","curLoc") != "curLoc"){
            val locationOfDefault = stationStringArrayList.indexOf(prefs.getString(prefs.getString("defaultLoc","curLoc"),""))
            if (locationOfDefault > 0){
                val defaultStation = stationStringArrayList[locationOfDefault]
                stationStringArrayList.remove(defaultStation)
                stationStringArrayList.add(0, defaultStation)
            }
        }


        adapter.notifyDataSetChanged()
        if (stationStringArrayList.size > 0) {
            val stat = try { stationStringArrayList[currentStation] }catch (e: Exception){stationStringArrayList[0]}
            updateStation(stat!!)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    class GetLocationData(
        private val latitude: Double, private var longitude: Double, preferences: SharedPreferences,
        private var stationList: ArrayList<String?>, private var position: Int
    ): AsyncTask<Void,Void,String>(){
        private var sharedPreferences = preferences
        override fun doInBackground(vararg params: Void?): String? {

            val url = URL("https://air.utah.gov/phoneApp.php?p=gps&lat=$latitude&long=$longitude")

            val httpClient = url.openConnection() as HttpsURLConnection
            try{
                if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        val stream = BufferedInputStream(httpClient.inputStream)
                        val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                        val stringBuilder = StringBuilder()
                        bufferedReader.forEachLine { stringBuilder.append(it) }
                        return stringBuilder.toString()
                    }catch (e: Exception){
                        e.printStackTrace()
                    }finally {
                        httpClient.disconnect()
                    }
                }
            }catch (ex: Exception){
                ex.printStackTrace()
            }finally {
                httpClient.disconnect()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try{
                val station = JSONObject(result)

                val stationObject: Station

                val forecast = Array(station.getJSONArray("forecast").length()){

                        i ->
                    Forecast(
                        station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                val temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                val windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    station.getJSONArray("station").getJSONObject(0).getString("id"),
                    station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                    station.getJSONArray("station").getJSONObject(0).getString("name"),
                    station.getJSONArray("station").getJSONObject(0).getString("label"),
                    station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("region"),
                    station.getString("seasonalParameter"),
                    Parameters(
                        temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                            ), Pollutant(
                                "pm25",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                "µg/m³",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                            )
                        )
                    ),
                    forecast
                )

                val editor = sharedPreferences.edit()

                editor.putString("curLoc",stationObject.jsonString())

                editor.apply()
                if ( position < stationList.size){
                    GetData(
                        JSONObject(stationList[position]).getJSONObject(
                            "station"
                        ).getString("id"), sharedPreferences, stationList, position + 1
                    ).execute()
                }else{

                }
            }catch (e: Exception){

            }

        }
    }

    class GetData(var station: String?, preferences: SharedPreferences,
                  var stationList: ArrayList<String?>, var position: Int
    ): AsyncTask<Void,Void,String>(){
        var sharedPreferences = preferences
        override fun doInBackground(vararg params: Void?): String? {
            if (station=="")
                station="slc"

            val url = URL("https://air.utah.gov/phoneApp.php?p=station&id=$station")


            val httpClient = url.openConnection() as HttpsURLConnection
            try{
                if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        val stream = BufferedInputStream(httpClient.inputStream)
                        val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                        val stringBuilder = StringBuilder()
                        bufferedReader.forEachLine { stringBuilder.append(it) }
                        return stringBuilder.toString()
                    }catch (e: Exception){
                        e.printStackTrace()
                    }finally {
                        httpClient.disconnect()
                    }
                }
            }catch (ex: Exception){
                ex.printStackTrace()
            }finally {
                httpClient.disconnect()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try{
                val station = JSONObject(result)

                var stationObject: Station
                var forecast: Array<Forecast>
                try {
                    forecast = Array(station.getJSONArray("forecast").length()){

                            i ->
                        Forecast(
                            station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                            station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                            station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                            station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                            station.getJSONArray("forecast").getJSONObject(i).getString("message")
                        )

                    }
                }catch (e: Exception){
                    forecast = emptyArray()
                }


                val temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                val windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                try {
                    stationObject = Station(
                        station.getJSONArray("station").getJSONObject(0).getString("id"),
                        station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                        station.getJSONArray("station").getJSONObject(0).getString("name"),
                        station.getJSONArray("station").getJSONObject(0).getString("label"),
                        station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                        station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                        station.getJSONArray("station").getJSONObject(0).getString("region"),
                        station.getString("seasonalParameter"),
                        Parameters(
                            temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                            arrayOf(
                                Pollutant(
                                    "ozone",
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                    station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                                ), Pollutant(
                                    "pm25",
                                    station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                    "µg/m³",
                                    station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                    station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                    station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                    station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                                )
                            )
                        ),
                        forecast
                    )
                }catch (e: Exception){
                    stationObject = Station("slc","slc","Salt Lake County","Hawthorn","0.000","0.000","Wastatch", "ozone",
                        Parameters(
                            temper, windSpeed, "N",
                            arrayOf(
                                Pollutant(
                                    "ozone",
                                    "0.000",
                                    "ppm",
                                    "0,0,0",
                                    "#FFFFFF",
                                    "",
                                    ""
                                ), Pollutant(
                                    "pm25",
                                    "0",
                                    "µg/m³",
                                    "0,0,0",
                                    "#FFFFFF",
                                    "",
                                    ""
                                )
                            )
                        ),forecast)
                }

                val editor = sharedPreferences.edit()

                editor.putString(stationObject.id,stationObject.jsonString())

                editor.apply()

                if ( position < stationList.size){
                    GetData(
                        JSONObject(stationList[position]).getJSONObject(
                            "station"
                        ).getString("id"), sharedPreferences, stationList, position + 1
                    ).execute()
                }
            }catch (ex: Exception){

            }
        }
    }
}

package org.autosciencetech.airquality

import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.station_item.view.*
import org.json.JSONObject
import java.lang.Exception

class FavoritesAdapter(private val stations: ArrayList<String?>, val mainActivity: MainActivity) : androidx.recyclerview.widget.RecyclerView.Adapter<FavoritesAdapter.SiteHolder>() {
    override fun getItemCount() = stations.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SiteHolder {
        return SiteHolder(
            parent.inflate(
                R.layout.station_item,
                false
            ), mainActivity
        )
    }

    override fun onBindViewHolder(holder: SiteHolder, position: Int) {
        val itemString = stations[position]
        holder.bindString(itemString!!)
    }

    class SiteHolder (v: View, private val mainActivity: MainActivity) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v), View.OnClickListener{
        private var view: View = v
        private var station: String? = null
        val PREFS_FILENAME = "org.autosciencetech.utahair"
        lateinit var prefs: SharedPreferences

        init {
            v.setOnClickListener(this)
        }
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onClick(v: View) {
            station?.let { mainActivity.updateStation(it) }
            val drawerLayout: DrawerLayout = mainActivity.findViewById(R.id.drawer_layout)
            drawerLayout.closeDrawer(GravityCompat.START)
        }

        fun bindString(station: String){
            prefs = mainActivity.getSharedPreferences(PREFS_FILENAME, 0)
            if (prefs.getString("curLoc","") == station){
                view.locationTagImageView.visibility = ImageView.VISIBLE
            }else
                view.locationTagImageView.visibility = ImageView.INVISIBLE

            this.station = station
            val stationJson = JSONObject(station)
            val stationName = stationJson.getJSONObject("station").getString("label") + " - " + stationJson.getJSONObject("station").getString("name")
            for(i in 0 until stationJson.getJSONObject("parameters").getJSONArray("pollutants").length()){
                when(stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("type")){
                    "ozone" ->{
                        view.ozoneValueTextView2.text = stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("value")
                        try {
                            view.ozoneValueTextView2.setTextColor(Color.parseColor("#"+stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("colorHex")))
                        }catch (e: Exception){
                            view.ozoneValueTextView2.setTextColor(Color.WHITE)
                        }
                    }
                    "pm25" ->{
                        view.pmValueTextView2.text = stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("value")
                        try {
                            view.pmValueTextView2.setTextColor(Color.parseColor("#"+stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("colorHex")))
                        }catch (e: Exception){
                            view.pmValueTextView2.setTextColor(Color.WHITE)
                        }
                    }
                    else->{}
                }
            }
            view.stationNameTextView.text = stationName

            if (prefs.getString("defaultLoc","") == stationJson.getJSONObject("station").getString("id")){
                view.locationTagImageView.visibility = ImageView.VISIBLE
                view.locationTagImageView.setImageResource(android.R.drawable.btn_star)
            }
        }
    }
}
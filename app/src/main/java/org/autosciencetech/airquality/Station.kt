package org.autosciencetech.airquality


data class Station(var id: String, val webSiteID: String, val name: String, val label: String, val latitude: String, val longitude: String, val region: String, val seasonalParameter: String, val parameters: Parameters, val forecast: Array<Forecast>?) {
    val PREFS_FILENAME = "org.autosciencetech.utahair"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Station

        if (id != other.id) return false
        if (webSiteID != other.webSiteID) return false
        if (name != other.name) return false
        if (label != other.label) return false
        if (latitude != other.latitude) return false
        if (longitude != other.longitude) return false
        if (region != other.region) return false
        if (seasonalParameter != other.seasonalParameter) return false
        if (parameters != other.parameters) return false
        if (forecast != null) {
            if (other.forecast != null)
                if (!forecast.contentEquals(other.forecast)) return false
        }

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + webSiteID.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + label.hashCode()
        result = 31 * result + latitude.hashCode()
        result = 31 * result + longitude.hashCode()
        result = 31 * result + region.hashCode()
        result = 31 * result + seasonalParameter.hashCode()
        result = 31 * result + parameters.hashCode()
        if (forecast != null) {
            result = 31 * result + forecast.contentHashCode()
        }
        return result
    }

    fun jsonString(): String{
        var jsonOutput = "{\"station\":" +
                            "{\"id\":\""+ id +"\"" +
                            ",\"websiteId\":\""+ webSiteID +"\"" +
                            ",\"name\":\""+ name +"\"" +
                            ",\"label\":\""+label+"\"" +
                            ",\"latitude\":\""+latitude+"\"" +
                            ",\"longitude\":\""+longitude+"\"" +
                            ",\"region\":\""+region+"\"}" +
                        ",\"parameters\":" +
                            "{\"pollutants\":["
                                for (param in parameters.pollutant){
                                    jsonOutput += "{\"type\":\""+param.type+"\"" +
                                    ",\"value\":\""+param.value+"\"" +
                                    ",\"unit\":\""+param.unit+"\"" +
                                    ",\"color\":\""+param.color+"\"" +
                                    ",\"colorHex\":\""+param.colorHex+"\"" +
                                    ",\"arrow\":\""+param.arrow+"\"" +
                                    ",\"range\":\""+param.range+"\"}"
                                    if (parameters.pollutant.indexOf(param) != parameters.pollutant.size-1){
                                        jsonOutput += ","
                                    }
                                }
                            jsonOutput += "]"+
                            ",\"temperature\":"+ parameters.temperature +
                            ",\"wind_speed\":"+ parameters.windSpeed +
                            ",\"wind_dir\":\""+parameters.windDir+"\"}" +
                        ",\"seasonalParameter\":\""+seasonalParameter+"\"" +
                        ",\"forecast\":" +
                            "["
        if (forecast != null) {
            for (fore in forecast){
                jsonOutput += "{\"date\":\""+fore.date +"\"" +
                        ",\"dayOfWeek\":\""+fore.dayOfWeek+"\"" +
                        ",\"severity\":\""+fore.severity+"\"" +
                        ",\"action\":\""+fore.action+"\"" +
                        ",\"message\":\""+fore.message+"\"}"
                if (forecast.indexOf(fore) != forecast.size-1){
                    jsonOutput += ","
                }
            }
        }
                            jsonOutput +="]}"

        return jsonOutput
    }
}


data class Parameters(val temperature: Int, val windSpeed: Double, val windDir: String, val pollutant: Array<Pollutant>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Parameters

        if (temperature != other.temperature) return false
        if (windSpeed != other.windSpeed) return false
        if (windDir != other.windDir) return false
        if (!pollutant.contentEquals(other.pollutant)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = temperature
        result = 31 * result + windSpeed.hashCode()
        result = 31 * result + windDir.hashCode()
        result = 31 * result + pollutant.contentHashCode()
        return result
    }
}

data class Pollutant(val type: String, val value: String, val unit: String, val color: String, val colorHex: String, val arrow: String, val range: String)

data class Forecast(val date: String, val dayOfWeek: String, val severity: String, val action: String, val message: String)
package org.autosciencetech.airquality

import android.annotation.TargetApi
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_settings.*
import org.json.JSONObject

class SettingsActivity : AppCompatActivity() {

    val PREFS_FILENAME = "org.autosciencetech.utahair"
    lateinit var prefs: SharedPreferences
    private lateinit var stationStringArrayList: ArrayList<String>
    lateinit var exampleTextView: TextView
    lateinit var redBackgroundSeekBar: SeekBar
    lateinit var greenBackgroundSeekBar: SeekBar
    lateinit var blueBackgroundSeekBar: SeekBar
    lateinit var alphaBackgroundSeekBar: SeekBar
    lateinit var redTextColorSeekBar: SeekBar
    lateinit var greenTextColorSeekBar: SeekBar
    lateinit var blueTextColorSeekBar: SeekBar
    lateinit var backgroundColor: Color
    lateinit var textColor: Color
    lateinit var cacheButton: Button
    lateinit var deleteButton: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)

        prefs = getSharedPreferences(PREFS_FILENAME, 0)

        stationStringArrayList = ArrayList<String>()

        val switch: Switch = findViewById(R.id.metricSwitch)
        switch.isChecked = prefs.getBoolean("UseMetric",false)
        switch.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            val editor = prefs.edit()
            editor.putBoolean("UseMetric",switch.isChecked)
            editor.apply()
        }

        for (site in prefs.getStringSet("favorites", HashSet<String>()).orEmpty()){
            stationStringArrayList.add(prefs.getString(site,"").orEmpty())
        }

        var stations: HashMap<String,String> = hashMapOf()
        stations["Current Location"] = "curLoc"

        for (site in stationStringArrayList){
            stations[JSONObject(site).getJSONObject("station").getString("label") + " - " + JSONObject(site).getJSONObject("station").getString("name")] = JSONObject(site).getJSONObject("station").getString("id")
        }

        val picker: NumberPicker = findViewById(R.id.defaultPicker)
        picker.minValue = 0
        picker.maxValue = stations.size-1
        picker.displayedValues = stations.keys.toTypedArray()

        if (!prefs.contains("defaultLoc")){
            val editor = prefs.edit()
            editor.putString("defaultLoc","curLoc")
            editor.apply()
        }
        if (!stations.containsValue(prefs.getString("defaultLoc","curLoc").orEmpty())){
            val editor = prefs.edit()
            editor.putString("defaultLoc","curLoc")
            editor.apply()
        }

        var value = stations.keys.toTypedArray().indexOf(stations.filterValues { it == prefs.getString("defaultLoc","curLoc") }.keys.first())
        picker.value = value

        picker.setOnValueChangedListener { _, _, newVal ->
            run {
                val key = stations[stations.keys.toTypedArray()[newVal]]
                val editor = prefs.edit()
                editor.putString("defaultLoc",key)
                editor.apply()
            }
        }

        exampleTextView = findViewById(R.id.demoTextView)
        redBackgroundSeekBar = findViewById(R.id.redBackgroundSeekBar)
        greenBackgroundSeekBar = findViewById(R.id.greenBackgroundSeekBar)
        blueBackgroundSeekBar = findViewById(R.id.blueBackgroundSeekBar)
        alphaBackgroundSeekBar = findViewById(R.id.alphaBackgroundSeekBar)
        redTextColorSeekBar = findViewById(R.id.redTextColorSeekBar)
        greenTextColorSeekBar = findViewById(R.id.greenTextColorSeekBar)
        blueTextColorSeekBar = findViewById(R.id.blueTextColorSeekBar)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            backgroundColor = Color.valueOf(
                prefs.getFloat("WidgetBackgroundRed", 0.0F),
                prefs.getFloat("WidgetBackgroundGreen", 0.0F),
                prefs.getFloat("WidgetBackgroundBlue", 0.0F),
                prefs.getFloat("WidgetBackgroundAlpha", 1.0F)
            )
            textColor = Color.valueOf(
                prefs.getFloat("WidgetTextRed", 1.0F),
                prefs.getFloat("WidgetTextGreen", 1.0F),
                prefs.getFloat("WidgetTextBlue", 1.0F),
                1.0F
            )

            exampleTextView.setTextColor(textColor.toArgb())
            exampleTextView.setBackgroundColor(backgroundColor.toArgb())
        }
        else{
            val backgroundRed = prefs.getFloat("WidgetBackgroundRed", 0.0F)*255
            val backgroundGreen = prefs.getFloat("WidgetBackgroundGreen", 0.0F)*255
            val backgroundBlue = prefs.getFloat("WidgetBackgroundBlue", 0.0F)*255
            val backgroundAlpha = prefs.getFloat("WidgetBackgroundAlpha", 1.0F)*255

            val textRed = prefs.getFloat("WidgetTextRed", 1.0F)*255
            val textGreen = prefs.getFloat("WidgetTextGreen", 1.0F)*255
            val textBlue = prefs.getFloat("WidgetTextBlue", 1.0F)*255

            exampleTextView.setTextColor(Color.rgb(textRed.toInt(),textGreen.toInt(),textBlue.toInt()))
            exampleTextView.setBackgroundColor(Color.argb(backgroundAlpha.toInt(), backgroundRed.toInt(), backgroundGreen.toInt(), backgroundBlue.toInt()))
        }
        redBackgroundSeekBar.progress = (prefs.getFloat("WidgetBackgroundRed", 0.0F)*1000.0F).toInt()
        greenBackgroundSeekBar.progress = (prefs.getFloat("WidgetBackgroundGreen", 0.0F)*1000.0F).toInt()
        blueBackgroundSeekBar.progress = (prefs.getFloat("WidgetBackgroundBlue", 0.0F)*1000.0F).toInt()
        alphaBackgroundSeekBar.progress = (prefs.getFloat("WidgetBackgroundAlpha", 1.0F)*1000.0F).toInt()
        redTextColorSeekBar.progress = (prefs.getFloat("WidgetTextRed", 1.0F)*1000.0F).toInt()
        greenTextColorSeekBar.progress = (prefs.getFloat("WidgetTextGreen", 1.0F)*1000.0F).toInt()
        blueTextColorSeekBar.progress = (prefs.getFloat("WidgetTextBlue", 1.0F)*1000.0F).toInt()



        redBackgroundSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateBackground()
            }
        })
        greenBackgroundSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateBackground()
            }
        })
        blueBackgroundSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateBackground()
            }
        })
        alphaBackgroundSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateBackground()
            }
        })

        redTextColorSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateText()
            }
        })
        greenTextColorSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateText()
            }
        })
        blueTextColorSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                updateText()
            }
        })

        cacheButton = findViewById(R.id.cacheButton)
        cacheButton.setOnClickListener { applicationContext.cacheDir.deleteRecursively() }

        deleteButton = findViewById(R.id.deleteButton)
        deleteButton.setOnClickListener { prefs.edit().clear().commit() }


    }

    private fun updateBackground(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            backgroundColor = Color.valueOf(
                redBackgroundSeekBar.progress.toFloat() / 1000.0F,
                greenBackgroundSeekBar.progress.toFloat() / 1000.0F,
                blueBackgroundSeekBar.progress.toFloat() / 1000.0F,
                alphaBackgroundSeekBar.progress.toFloat() / 1000.0F
            )
            exampleTextView.setBackgroundColor(backgroundColor.toArgb())
        }else{
            exampleTextView.setBackgroundColor(Color.argb(
                ((alphaBackgroundSeekBar.progress.toFloat()/1000.0F)*255.0F).toInt(),
                ((redBackgroundSeekBar.progress.toFloat()/1000.0F)*255.0F).toInt(),
                ((greenBackgroundSeekBar.progress.toFloat()/1000.0F)*255.0F).toInt(),
                ((blueBackgroundSeekBar.progress.toFloat()/1000.0F)*255.0F).toInt()
            ))
        }


        val editor = prefs.edit()
        editor.putFloat("WidgetBackgroundRed",redBackgroundSeekBar.progress.toFloat()/1000.0F)
        editor.putFloat("WidgetBackgroundGreen",greenBackgroundSeekBar.progress.toFloat()/1000.0F)
        editor.putFloat("WidgetBackgroundBlue",blueBackgroundSeekBar.progress.toFloat()/1000.0F)
        editor.putFloat("WidgetBackgroundAlpha",alphaBackgroundSeekBar.progress.toFloat()/1000.0F)
        editor.apply()

        val manager = AppWidgetManager.getInstance(this)
        val ids = manager.getAppWidgetIds(ComponentName(this,
            UtahAirWidget::class.java))
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids)
        sendBroadcast(updateIntent)
    }

    private fun updateText(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textColor = Color.valueOf(
                redTextColorSeekBar.progress.toFloat() / 1000.0F,
                greenTextColorSeekBar.progress.toFloat() / 1000.0F,
                blueTextColorSeekBar.progress.toFloat() / 1000.0F,
                1.0F
            )
            exampleTextView.setTextColor(textColor.toArgb())
        }else{
            exampleTextView.setTextColor(Color.rgb(
                ((redTextColorSeekBar.progress.toFloat() / 1000.0F)*255.0F).toInt(),
                ((greenTextColorSeekBar.progress.toFloat() / 1000.0F)*255.0F).toInt(),
                ((blueTextColorSeekBar.progress.toFloat() / 1000.0F)*255.0F).toInt()
            ))
        }

        val editor = prefs.edit()
        editor.putFloat("WidgetTextRed",redTextColorSeekBar.progress.toFloat()/1000.0F)
        editor.putFloat("WidgetTextGreen",greenTextColorSeekBar.progress.toFloat()/1000.0F)
        editor.putFloat("WidgetTextBlue",blueTextColorSeekBar.progress.toFloat()/1000.0F)
        editor.apply()

        val manager = AppWidgetManager.getInstance(this)
        val ids = manager.getAppWidgetIds(ComponentName(this,
            UtahAirWidget::class.java))
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids)
        sendBroadcast(updateIntent)
    }
}

package org.autosciencetech.airquality

import android.Manifest
import android.app.job.JobParameters
import android.app.job.JobService
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.google.android.gms.wearable.*
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.lang.Exception
import java.lang.StringBuilder
import java.net.URL
import javax.net.ssl.HttpsURLConnection



class UpdateService : JobService() {
    val PREFS_FILENAME = "org.autosciencetech.utahair"
    var stationList: ArrayList<String> = ArrayList()
    var preferences: SharedPreferences? = null
    var taskm: AsyncTask<Void,Void,String>? = null
    override fun onStopJob(params: JobParameters?): Boolean {
        if (taskm != null){
            taskm!!.cancel(true)
            Toast.makeText(this,"The Service has been canceled",Toast.LENGTH_SHORT).show()
        }
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        preferences = this.getSharedPreferences(PREFS_FILENAME,0)
        taskm = GetFavoritesData(
            this,
            "slc",
            stationList,
            -1,
            preferences!!
        )
        updateStations(params)
        return true
    }



    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun updateStations(params: JobParameters?){

        var fusedLocationClient: FusedLocationProviderClient

        if (preferences == null){
            preferences = getSharedPreferences(PREFS_FILENAME,0)
        }

        if(checkPermissions()) {
            var latitude: Double
            var longitude: Double
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnCompleteListener { task -> if (task.isSuccessful && task.result != null){
                    val lastLocation = task.result
                    latitude = lastLocation!!.latitude
                    longitude = lastLocation.longitude
                    //Toast.makeText(this,"Getting Location Data",Toast.LENGTH_SHORT).show()
                    if (isConnectedToNetwork())
                        taskm = GetLocationData(
                            this,
                            latitude,
                            longitude,
                            getSharedPreferences(PREFS_FILENAME, 0)
                        ).execute()
                } else{
                    if (preferences!!.getStringSet("favorites",null) != null && preferences!!.getStringSet(
                            "favorites",
                            null
                        ).orEmpty().isNotEmpty()
                    ) {
                        for (site in preferences!!.getStringSet("favorites", HashSet<String>()).orEmpty()) {
                            stationList.add(preferences!!.getString(site, null).orEmpty())
                        }
                    }
                    if (isConnectedToNetwork())
                        taskm = GetFavoritesData(
                            this,
                            "slc",
                            stationList,
                            0,
                            preferences!!
                        )
                //Toast.makeText(this,"Getting Location Data Failed Getting Favorites",Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            if (preferences!!.getStringSet("favorites",null) != null && preferences!!.getStringSet("favorites",null).orEmpty().isNotEmpty()) {
                for (site in preferences!!.getStringSet("favorites", HashSet<String>()).orEmpty()) {
                    stationList.add(preferences!!.getString(site, "slc").orEmpty())
                }
            }else{
                stationList.add("slc")
            }
            if (isConnectedToNetwork())
                taskm = GetFavoritesData(
                    this,
                    "slc",
                    stationList,
                    0,
                    preferences!!
                )
            //Toast.makeText(this,"Getting Getting Favorites",Toast.LENGTH_SHORT).show()
        }

        //taskm!!.execute()

        //while (taskm.status == AsyncTask.Status.RUNNING) continue

        jobFinished(params,true)
    }

    private class GetLocationData(context: Context, latitude: Double, longitude: Double, sharedPreferences: SharedPreferences): AsyncTask<Void, Void, String>(){
        val latitude = latitude
        val longitude = longitude
        val context = context

        val prefs = sharedPreferences
        override fun doInBackground(vararg params: Void?): String? {

            val url = URL("https://air.utah.gov/phoneApp.php?p=gps&lat=$latitude&long=$longitude")

            if (context.isConnectedToNetwork()){
                val httpClient = url.openConnection() as HttpsURLConnection
                try  {
                    if((httpClient.responseCode == HttpsURLConnection.HTTP_OK)){
                        try {
                            val stream = BufferedInputStream(httpClient.inputStream)
                            val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                            val stringBuilder = StringBuilder()
                            bufferedReader.forEachLine { stringBuilder.append(it) }
                            return stringBuilder.toString()
                        }catch (e: Exception){
                            e.printStackTrace()
                        }finally {
                            httpClient.disconnect()
                        }
                    }
                }catch (ex: Exception){
                    ex.printStackTrace()
                }finally {
                    httpClient.disconnect()
                }
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try{
                val station = JSONObject(result)
                var stationObject: Station

                var forecast = Array(station.getJSONArray("forecast").length()){

                        i ->
                    Forecast(
                        station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                var temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                var windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    station.getJSONArray("station").getJSONObject(0).getString("id"),
                    station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                    station.getJSONArray("station").getJSONObject(0).getString("name"),
                    station.getJSONArray("station").getJSONObject(0).getString("label"),
                    station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("region"),
                    station.getString("seasonalParameter"),
                    Parameters(
                        temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                            ), Pollutant(
                                "pm25",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                "µg/m³",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                            )
                        )
                    ),
                    forecast
                )

                val editor = prefs.edit()

                editor.putString("curLoc",stationObject.jsonString())

                editor.commit()

                if (result != null) {

                    var dataClient: DataClient = Wearable.getDataClient(context)
                    //Toast.makeText(context,"Created Data Client",Toast.LENGTH_LONG).show()

                    val putDataReq: PutDataRequest = PutDataMapRequest.create("/station").run {
                        dataMap.putString("org.autosciencetech.utahair.station",stationObject.jsonString())
                        asPutDataRequest()
                    }
                    putDataReq.setUrgent()
                    val putDataTask: Task<DataItem> = dataClient.putDataItem(putDataReq)


                    putDataTask.addOnSuccessListener {  }
                    putDataTask.addOnFailureListener {  }

                }

                val manager = AppWidgetManager.getInstance(context)
                val ids = manager.getAppWidgetIds(ComponentName(context,
                    UtahAirWidget::class.java))
                val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
                updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids)
                context.sendBroadcast(updateIntent)
            }catch (e: Exception){

            }
        }
    }

    private class GetFavoritesData(context: Context, stationID: String, stationList: ArrayList<String>, position: Int , sharedPreferences: SharedPreferences): AsyncTask<Void, Void, String>(){
        var station = stationID
        val stationList = stationList
        val position = position
        val context = context

        val prefs = sharedPreferences
        override fun doInBackground(vararg params: Void?): String? {

            if (station == "" || position == -1){
                station = "slc"
            }

            val url = URL("https://air.utah.gov/phoneApp.php?p=station&id=$station")

            if (context.isConnectedToNetwork()){
                val httpClient = url.openConnection() as HttpsURLConnection
                try {
                    if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                        try {
                            val stream = BufferedInputStream(httpClient.inputStream)
                            val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                            val stringBuilder = StringBuilder()
                            bufferedReader.forEachLine { stringBuilder.append(it) }
                            return stringBuilder.toString()
                        }catch (e: Exception){
                            e.printStackTrace()
                        }finally {
                            httpClient.disconnect()
                        }
                    }
                }catch (ex: Exception){
                    ex.printStackTrace()
                }finally {
                    httpClient.disconnect()
                }
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                val station = JSONObject(result)

                var stationObject: Station

                var forecast = Array(station.getJSONArray("forecast").length()){

                        i ->
                    Forecast(
                        station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                var temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                var windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    station.getJSONArray("station").getJSONObject(0).getString("id"),
                    station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                    station.getJSONArray("station").getJSONObject(0).getString("name"),
                    station.getJSONArray("station").getJSONObject(0).getString("label"),
                    station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("region"),
                    station.getString("seasonalParameter"),
                    Parameters(
                        temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                            ), Pollutant(
                                "pm25",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                "µg/m³",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                            )
                        )
                    ),
                    forecast
                )

                val editor = prefs.edit()

                if (position > 0)
                    editor.putString("curLoc",stationObject.jsonString())
                else
                    editor.putString(stationObject.id,stationObject.jsonString())

                editor.commit()

                if (stationList.size > position+1){
                    GetFavoritesData(
                        context,
                        stationList[position + 1],
                        stationList,
                        position + 1,
                        prefs
                    ).execute()
                }

                Toast.makeText(context,"Attempting null check",Toast.LENGTH_SHORT).show()
                if (result != null) {
                    Toast.makeText(context,"Null check passed",Toast.LENGTH_SHORT).show()
                    sendData(stationObject.jsonString())
                }

                val manager = AppWidgetManager.getInstance(context)
                val intent = Intent(context, UtahAirWidget::class.java)
                val appWidgetIds = manager.getAppWidgetIds(ComponentName(context.applicationContext,
                    UtahAirWidget::class.java))
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds)
                context.sendBroadcast(intent)
            }catch (e: Exception){

            }
        }

        fun sendData(stationData: String){
            var dataClient: DataClient = Wearable.getDataClient(context)
            dataClient.asGoogleApiClient().connect()

            val putDataReq: PutDataRequest = PutDataMapRequest.create("/station").run {
                dataMap.putString("org.autosciencetech.utahair.station",stationData)
                asPutDataRequest()
            }
            putDataReq.setUrgent()
            val putDataTask: Task<DataItem> = dataClient.putDataItem(putDataReq)

            dataClient.asGoogleApiClient().disconnect()
        }
    }


}
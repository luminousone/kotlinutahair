package org.autosciencetech.airquality

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.content_message.*

class PopUpWindow : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_message)
        closeMessageButton.setOnClickListener { finish() }
        messageTextView.text = intent.getStringExtra("Message")
    }
}

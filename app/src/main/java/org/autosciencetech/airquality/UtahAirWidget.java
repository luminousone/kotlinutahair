package org.autosciencetech.airquality;

import android.Manifest;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of App Widget functionality.
 */
public class UtahAirWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        String widgetText = "";
        SharedPreferences preferences = context.getSharedPreferences("org.autosciencetech.utahair",0);
        FusedLocationProviderClient locationProviderClient;
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.utah_air_widget);
        //views.setTextViewText(R.id.widgetUpdatedTextView, widgetText);
        //views.setImageViewResource(R.id.conditionImageView,R.drawable.mandatory);
        views.setImageViewResource(R.id.settingsImageButton,android.R.drawable.ic_menu_manage);
        views.setImageViewResource(R.id.refreshImageButton,android.R.drawable.stat_notify_sync);
        views.setTextViewText(R.id.widgetPollutantTextView,"");
        views.setTextViewText(R.id.widgetUpdatedTextView,"");

        int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(context,UtahAirWidget.class));
        Intent updateIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,1,updateIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.refreshImageButton,pendingIntent);

        Intent launchIntent = new Intent(context,WelcomeActivity.class);
        PendingIntent pendingLaunchIntent = PendingIntent.getActivity(context,2,launchIntent,0);
        views.setOnClickPendingIntent(R.id.dataLayout,pendingLaunchIntent);

        Intent settingsIntent = new Intent(context,SettingsActivity.class);
        settingsIntent.putExtra("WidgetID",appWidgetId);
        PendingIntent pendingSettingsIntent = PendingIntent.getActivity(context,3,settingsIntent,0);
        views.setOnClickPendingIntent(R.id.settingsImageButton,pendingSettingsIntent);

        if (checkPermissions(context) && (preferences.contains("defaultLoc") && preferences.getString("defaultLoc","curLoc").equals("curLoc") || !preferences.contains("defaultLoc"))){
            views.setTextViewText(R.id.widgetLocationTextView,"Collecting Available");
            /*locationProviderClient = LocationServices.getFusedLocationProviderClient(context);
            locationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    try {
                        JSONObject text = new JSONObject(getCurrentLocation(task.getResult().getLatitude(),task.getResult().getLongitude()));
                        views.setTextViewText(R.id.widgetLocationTextView,text.getJSONObject("station").getString("name"));
                    } catch (JSONException e) {
                        views.setTextViewText(R.id.widgetLocationTextView,"No Data Available");
                    }

                }
            });*/
            widgetText = preferences.getString("curLoc","");


            try {

                updateViews(views,new JSONObject(widgetText),preferences);

            } catch (JSONException e) {
                views.setTextViewText(R.id.widgetLocationTextView,"No Data Available");
            }
        }else if (!checkPermissions(context) && preferences.contains("defaultLoc") && preferences.getString("defaultLoc","curLoc").equals("curLoc")){
            views.setTextViewText(R.id.widgetLocationTextView,"No location Data Available");
        }else if(preferences.contains("defaultLoc") && preferences.contains(preferences.getString("defaultLoc","curLoc")) && !preferences.getString("defaultLoc","curLoc").equals("curLoc")){
            widgetText = preferences.getString(preferences.getString("defaultLoc","curLoc"),"");
            views.setTextViewText(R.id.widgetLocationTextView,"No default Available");
            try {
                updateViews(views,new JSONObject(widgetText),preferences);
            } catch (JSONException e) {
                views.setTextViewText(R.id.widgetLocationTextView,"No Data Available");
            }
        }else {
            try {
                widgetText = preferences.getString("curLoc","");
                updateViews(views,new JSONObject(widgetText),preferences);
            } catch (Exception e) {
                views.setTextViewText(R.id.widgetLocationTextView,"No Data Available");
            }
        }

        Color backgroundColor;
        Color textColor;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            backgroundColor = Color.valueOf(preferences.getFloat("WidgetBackgroundRed", 0.0F), preferences.getFloat("WidgetBackgroundGreen", 0.0F), preferences.getFloat("WidgetBackgroundBlue", 0.0F), preferences.getFloat("WidgetBackgroundAlpha", 1.0F));
            textColor = Color.valueOf(preferences.getFloat("WidgetTextRed", 1.0F), preferences.getFloat("WidgetTextGreen", 1.0F), preferences.getFloat("WidgetTextBlue", 1.0F), 1.0F);

            views.setTextColor(R.id.widgetUpdatedTextView,textColor.toArgb());
            views.setTextColor(R.id.widgetPollutantTextView,textColor.toArgb());
            views.setTextColor(R.id.widgetLocationTextView,textColor.toArgb());
            views.setInt(R.id.widgetLayout, "setBackgroundColor",backgroundColor.toArgb());
        }else {
            int backgroundRed = (int) (((preferences.getFloat("WidgetBackgroundRed", 0.0F)))*255.0F);
            int backgroundGreen = (int) (((preferences.getFloat("WidgetBackgroundGreen", 0.0F)))*255.0F);
            int backgroundBlue = (int) (((preferences.getFloat("WidgetBackgroundBlue", 0.0F)))*255.0F);
            int backgroundAlpha = (int) (((preferences.getFloat("WidgetBackgroundAlpha", 1.0F)))*255.0F);

            int textRed = (int)(((preferences.getFloat("WidgetTextRed", 1.0F)))*255.0F);
            int textGreen = (int)(((preferences.getFloat("WidgetTextGreen", 1.0F)))*255.0F);
            int textBlue = (int)(((preferences.getFloat("WidgetTextBlue", 1.0F)))*255.0F);

            views.setTextColor(R.id.widgetUpdatedTextView,Color.rgb(textRed,textGreen,textBlue));
            views.setTextColor(R.id.widgetPollutantTextView,Color.rgb(textRed,textGreen,textBlue));
            views.setTextColor(R.id.widgetLocationTextView,Color.rgb(textRed,textGreen,textBlue));
            views.setInt(R.id.widgetLayout, "setBackgroundColor",Color.argb(backgroundAlpha,backgroundRed,backgroundGreen,backgroundBlue));
        }

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private static Boolean checkPermissions(Context context) {
        int permissionState = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private static void updateViews(RemoteViews views, JSONObject station, SharedPreferences preferences){
        try {
            views.setTextViewText(R.id.widgetLocationTextView,station.getJSONObject("station").getString("name"));

            String pollutantType = preferences.getString("pollutant", station.getString("seasonalParameter"));
            for (int i = 0 ; i< station.getJSONObject("parameters").getJSONArray("pollutants").length();i++){
                if (station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("type").equals(pollutantType)){
                    views.setTextViewText(R.id.widgetPollutantTextView,station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("value") + station.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("unit"));
                }
            }
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm a M/d");
            views.setTextViewText(R.id.widgetUpdatedTextView, sdf.format(new Date()));

            if(station.getJSONArray("forecast").length() > 0){
                switch (station.getJSONArray("forecast").getJSONObject(0).getString("action")){
                    case "Unrestricted":
                        views.setImageViewResource(R.id.conditionImageView,R.drawable.unrestricted);
                        break;
                    case "Voluntary":
                        views.setImageViewResource(R.id.conditionImageView,R.drawable.voluntary);
                        break;
                    default:
                        views.setImageViewResource(R.id.conditionImageView,R.drawable.mandatory);
                }
            }

        } catch (JSONException e) {
            views.setTextViewText(R.id.widgetLocationTextView,"No Data Available");
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        JobScheduler jobScheduler = (JobScheduler) context.getApplicationContext().getSystemService(Context.JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(context,UpdateService.class);
        JobInfo jobInfo;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            jobInfo = new JobInfo.Builder(1337, componentName).setPeriodic(TimeUnit.MINUTES.toMillis(30), TimeUnit.MINUTES.toMillis(15)).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setRequiresBatteryNotLow(true).setBackoffCriteria(10000, JobInfo.BACKOFF_POLICY_LINEAR).setPersisted(true).build();
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            jobInfo = new JobInfo.Builder(1337,componentName).setPeriodic(TimeUnit.MINUTES.toMillis(30),TimeUnit.MINUTES.toMillis(15)).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setBackoffCriteria(10000,JobInfo.BACKOFF_POLICY_LINEAR).setPersisted(true).build();
        }else {
            jobInfo = new JobInfo.Builder(1337,componentName).setPeriodic(TimeUnit.MINUTES.toMillis(30)).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setBackoffCriteria(10000,JobInfo.BACKOFF_POLICY_LINEAR).setPersisted(true).build();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (jobScheduler.getPendingJob(1337) == null){
                jobScheduler.schedule(jobInfo);
            }
        }else {
            jobScheduler.schedule(jobInfo);
        }

        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}


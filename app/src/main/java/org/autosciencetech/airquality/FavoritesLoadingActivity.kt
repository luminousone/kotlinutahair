package org.autosciencetech.airquality

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_favorites_loading.toolbar
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.lang.Exception
import java.lang.StringBuilder
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class FavoritesLoadingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites_loading)
        setSupportActionBar(toolbar)

        GetAllStationData(this).execute()
    }

    class GetAllStationData(activity: FavoritesLoadingActivity): AsyncTask<Void, Void, String>(){
        private val thisActivity = activity
        private val loadingStatus: TextView = thisActivity.findViewById(R.id.progressTextView)
        override fun doInBackground(vararg params: Void?): String? {
            val url = URL("https://air.utah.gov/phoneApp.php?p=allstationlist")
            val httpClient = url.openConnection() as HttpsURLConnection
            if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                try {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                    val stringBuilder = StringBuilder()
                    bufferedReader.forEachLine { stringBuilder.append(it) }
                    return stringBuilder.toString()
                }catch (e: Exception){
                    e.printStackTrace()
                }finally {
                    httpClient.disconnect()
                }
            }
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            loadingStatus.text = "Gathering Station Data"
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            var stationArray = JSONObject(result).getJSONArray("allstationlist")
            var stationArrayList: ArrayList<JSONObject> = ArrayList()
            for (i in 0 until stationArray.length())
                stationArrayList.add(stationArray.getJSONObject(i))

            stationArrayList.sortWith(compareBy({it.getJSONObject("station").getString("region")},{it.getJSONObject("station").getString("label")},{it.getJSONObject("station").getString("name")}))

            val intent = Intent(thisActivity, FavoritesPageActivity::class.java)
            var stationStringArrayList: ArrayList<String> = ArrayList()

            var currentRegion = stationArrayList.get(0).getJSONObject("station").getString("region")
            stationStringArrayList.add(currentRegion)

            for(station in stationArrayList){
                if (currentRegion == station.getJSONObject("station").getString("region"))
                    stationStringArrayList.add(station.toString())
                else{
                    currentRegion = station.getJSONObject("station").getString("region")
                    stationStringArrayList.add(currentRegion)
                    stationStringArrayList.add(station.toString())
                }
            }

            intent.putStringArrayListExtra("stationList",stationStringArrayList)
            thisActivity.startActivity(intent)
        }
    }

    class GetRegionData(activity: FavoritesLoadingActivity): AsyncTask<Void, Void, String>(){
        private val thisActivity = activity
        private val loadingStatus: TextView = thisActivity.findViewById(R.id.progressTextView)
        override fun doInBackground(vararg params: Void?): String? {
            val url = URL("https://air.utah.gov/phoneApp.php?p=region")
            val httpClient = url.openConnection() as HttpsURLConnection
            if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                try {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                    val stringBuilder = StringBuilder()
                    bufferedReader.forEachLine { stringBuilder.append(it) }
                    return stringBuilder.toString()
                }catch (e: Exception){
                    e.printStackTrace()
                }finally {
                    httpClient.disconnect()
                }
            }
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            loadingStatus.text = "Gathering Regions"
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            GetSiteData(thisActivity, result).execute()
        }
    }

    class GetSiteData(activity: FavoritesLoadingActivity, regionData: String?): AsyncTask<Void, Void, String>(){
        private val thisActivity = activity
        private val regions = regionData
        private val loadingStatus: TextView = thisActivity.findViewById(R.id.progressTextView)
        override fun doInBackground(vararg params: Void?): String? {
            val regionData: JSONObject = JSONObject(regions)
            val region = regionData.getJSONArray("regions")

            var regionProcessedData: JSONObject = JSONObject()
            for (i in 0 until region.length()){
                val currentRegionData = regionData.getJSONObject("stationList").getJSONArray(region.getString(i))
                for (x in 0 until currentRegionData.length()){
                    val url = URL("https://air.utah.gov/phoneApp.php?p=station&id=" + currentRegionData.getString(x))
                    val httpClient = url.openConnection() as HttpsURLConnection
                    if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                        try {
                            val stream = BufferedInputStream(httpClient.inputStream)
                            val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                            val stringBuilder = StringBuilder()
                            bufferedReader.forEachLine { stringBuilder.append(it) }
                            regionProcessedData.put(region.get(i).toString(),JSONObject(stringBuilder.toString()))
                        }catch (e: Exception){
                            e.printStackTrace()
                        }finally {
                            httpClient.disconnect()
                        }
                    }
                }
            }
            return regionProcessedData.toString()
        }

        override fun onPreExecute() {
            super.onPreExecute()
            loadingStatus.text = "Gathering Site Data"
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            val intent = Intent(thisActivity, FavoritesPageActivity::class.java)
            intent.putExtra("stationList",result)
            thisActivity.startActivity(intent)
        }
    }
}

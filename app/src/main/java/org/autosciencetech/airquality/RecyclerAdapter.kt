package org.autosciencetech.airquality

import android.content.SharedPreferences
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.favorites_header.view.*
import kotlinx.android.synthetic.main.favorites_item.view.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception

class RecyclerAdapter(private val stations: ArrayList<String>, val preferences: SharedPreferences) : androidx.recyclerview.widget.RecyclerView.Adapter<RecyclerAdapter.StationHolder>() {
    override fun getItemCount() = stations.size

    override fun onBindViewHolder(holder: StationHolder, position: Int) {
        val itemString = stations[position]
        holder.bindString(itemString, getItemViewType(position), preferences)
    }

    override fun getItemViewType(position: Int): Int {
        super.getItemViewType(position)
        var station = stations[position]

        if (station.length < 20)
            return 0
        else
            return 1
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationHolder {
        if (viewType == 1)
            return StationHolder(
                parent.inflate(
                    R.layout.favorites_item,
                    false
                )
            )
        else
            return StationHolder(
                parent.inflate(
                    R.layout.favorites_header,
                    false
                )
            )


    }

    class StationHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v), View.OnClickListener {
        private var header: String? = null
        private var view: View = v
        private var station: String? = null
        private lateinit var stationObject: Station
        private lateinit var prefs: SharedPreferences
        private lateinit var favorites: HashSet<String>

        init {
            v.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            try {
                view.favoriteSwitch.toggle()
                if(favorites.contains(stationObject.id))
                    favorites.remove(stationObject.id)
                else
                    favorites.add(stationObject.id)

                val editor = prefs.edit()
                editor.putStringSet("favorites",favorites)
                if(favorites.contains(stationObject.id))
                    editor.putString(stationObject.id,stationObject.jsonString())
                else
                    editor.remove(stationObject.id)
                editor.commit()
            }catch (e:Exception){

            }
        }

        fun bindString(station: String, viewType: Int, preferences: SharedPreferences){
            if (viewType == 0){
                this.header = station
                view.headerTextView.text = header
            }else{
                this.station = station
                this.prefs = preferences
                this.favorites = prefs.getStringSet("favorites",HashSet<String>()) as HashSet<String>
                val stationJson = JSONObject(station)
                val stationName = stationJson.getJSONObject("station").getString("label") + " - " + stationJson.getJSONObject("station").getString("name")
                view.locationNameTextView.text = stationName
                view.pmValueTextView.text = stationJson.getJSONObject("parameters").getJSONObject("pm25").getString("value")
                view.pmValueTextView.setTextColor(Color.parseColor("#"+stationJson.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex")))
                view.ozoneValueTextView.text = stationJson.getJSONObject("parameters").getJSONObject("ozone").getString("value")
                view.ozoneValueTextView.setTextColor(Color.parseColor("#"+stationJson.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex")))

                var forecast = Array(stationJson.getJSONArray("forecast").length()){

                    i ->
                    Forecast(
                        stationJson.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        stationJson.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        stationJson.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        stationJson.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        stationJson.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                var temperature = try {
                    stationJson.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                var windSpeed = try{
                    stationJson.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    stationJson.getJSONObject("station").getString("id"),
                    stationJson.getJSONObject("station").getString("websiteId"),
                    stationJson.getJSONObject("station").getString("name"),
                    stationJson.getJSONObject("station").getString("label"),
                    stationJson.getJSONObject("station").getString("latitude"),
                    stationJson.getJSONObject("station").getString("longitude"),
                    stationJson.getJSONObject("station").getString("region"),
                    stationJson.getString("seasonalParameter"),
                    Parameters(
                        temperature,
                        windSpeed,
                        stationJson.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "value"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "unit"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "color"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "colorHex"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "arrow"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("ozone").getString(
                                    "range"
                                )
                            ), Pollutant(
                                "pm25",
                                stationJson.getJSONObject("parameters").getJSONObject("pm25").getString(
                                    "value"
                                ),
                                "µg/m³",
                                stationJson.getJSONObject("parameters").getJSONObject("pm25").getString(
                                    "color"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("pm25").getString(
                                    "colorHex"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("pm25").getString(
                                    "arrow"
                                ),
                                stationJson.getJSONObject("parameters").getJSONObject("pm25").getString(
                                    "range"
                                )
                            )
                        )
                    ),
                    forecast
                )

                view.favoriteSwitch.isChecked = favorites.contains(stationObject.id)
            }
        }
    }
}
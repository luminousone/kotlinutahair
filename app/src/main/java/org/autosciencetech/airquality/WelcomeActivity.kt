package org.autosciencetech.airquality

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class WelcomeActivity : AppCompatActivity() {

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    private var fusedLocationClient: FusedLocationProviderClient? = null
    val PREFS_FILENAME = "org.autosciencetech.utahair"
    lateinit var prefs: SharedPreferences
    private lateinit var stationStringArrayList: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        prefs = this.getSharedPreferences(PREFS_FILENAME, 0)
        stationStringArrayList = ArrayList<String>()

        for (site in prefs.getStringSet("favorites", HashSet<String>()).orEmpty()){
            stationStringArrayList.add(prefs.getString(site,null).orEmpty())
        }

        val progressBar: ProgressBar = findViewById(R.id.dataProgressBar)
        progressBar.max = stationStringArrayList.size+1

        if(checkPermissions()) {
            var latitude: Double
            var longitude: Double
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationClient!!.lastLocation.addOnCompleteListener { task -> if (task.isSuccessful && task.result != null){
                    val lastLocation = task.result
                    latitude = lastLocation!!.latitude
                    longitude = lastLocation.longitude

                    if(isConnectedToNetwork())
                        GetLocationData(
                            this,
                            latitude,
                            longitude,
                            prefs,
                            stationStringArrayList,
                            0
                        ).execute()
                    else{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                } else{
                    if (stationStringArrayList.size <= 0){
                        if(isConnectedToNetwork())
                            GetFavoritesData(
                                this,
                                "slc",
                                prefs,
                                stationStringArrayList,
                                0
                            ).execute()
                        else{
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        }
                    }else
                        if(isConnectedToNetwork())
                            GetFavoritesData(
                                this,
                                JSONObject(stationStringArrayList[0]).getJSONObject("station").getString(
                                    "id"
                                ),
                                prefs,
                                stationStringArrayList,
                                0
                            ).execute()
                        else{
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        }
                }
            }
        }else{

                requestPermissions()

        }
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {

            val builder =  AlertDialog.Builder(this)
            builder.setMessage(R.string.permission_rationale).setTitle("Permission Required")

            builder.setPositiveButton("ok"){
                    _, _ -> startLocationPermissionRequest()
            }

            val dialog = builder.create()
            dialog.show()

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                if (stationStringArrayList.size <= 0){
                    if(isConnectedToNetwork())
                        GetFavoritesData(
                            this,
                            "slc",
                            prefs,
                            stationStringArrayList,
                            0
                        ).execute()
                    else{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                }else
                    if(isConnectedToNetwork())
                        GetFavoritesData(
                            this,
                            JSONObject(stationStringArrayList[0]).getJSONObject("station").getString(
                                "id"
                            ),
                            prefs,
                            stationStringArrayList,
                            0
                        ).execute()
                    else{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
            } else{
                var latitude: Double
                var longitude: Double
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                fusedLocationClient!!.lastLocation.addOnCompleteListener { task -> if (task.isSuccessful && task.result != null){
                    val lastLocation = task.result
                    latitude = lastLocation!!.latitude
                    longitude = lastLocation.longitude
                    if (isConnectedToNetwork()){
                        GetLocationData(
                            this,
                            latitude,
                            longitude,
                            prefs,
                            stationStringArrayList,
                            0
                        ).execute()
                    }
                    else{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                } else{
                    if(isConnectedToNetwork()){
                        if (stationStringArrayList.size <= 0){
                            GetFavoritesData(
                                this,
                                "slc",
                                prefs,
                                stationStringArrayList,
                                0
                            ).execute()
                        }else
                            GetFavoritesData(
                                this,
                                JSONObject(stationStringArrayList[0]).getJSONObject("station").getString(
                                    "id"
                                ),
                                prefs,
                                stationStringArrayList,
                                0
                            ).execute()
                    }
                    else{
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
                }
            }
        }

    }

    class GetLocationData(activity: WelcomeActivity, latitude: Double, longitude: Double, preferences: SharedPreferences, stationList: ArrayList<String>, position: Int): AsyncTask<Void,Void,String>(){
        var sharedPreferences = preferences
        var stationList = stationList
        var position = position
        val latitude = latitude
        var longitude = longitude
        var activity = activity
        override fun doInBackground(vararg params: Void?): String? {

            val url = URL("https://air.utah.gov/phoneApp.php?p=gps&lat=$latitude&long=$longitude")


            val httpClient = url.openConnection() as HttpsURLConnection
            try{
                if (httpClient.responseCode == HttpsURLConnection.HTTP_OK) {
                    try {
                        val stream = BufferedInputStream(httpClient.inputStream)
                        val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                        val stringBuilder = StringBuilder()
                        bufferedReader.forEachLine { stringBuilder.append(it) }
                        return stringBuilder.toString()

                    }catch (e: Exception){
                        Snackbar.make(
                            activity.findViewById(R.id.dataProgressBar),
                            "Unable to connect to network",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }finally {
                        httpClient.disconnect()
                    }
                }
            }catch (ex: Exception){
                Snackbar.make(
                    activity.findViewById(R.id.dataProgressBar),
                    "Unable to connect to network",
                    Snackbar.LENGTH_INDEFINITE
                ).show()
            }finally {
                httpClient.disconnect()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            try{
                val station = JSONObject(result)

                val stationObject: Station

                val forecast = Array(station.getJSONArray("forecast").length()){

                        i ->
                    Forecast(
                        station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                val temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                val windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    station.getJSONArray("station").getJSONObject(0).getString("id"),
                    station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                    station.getJSONArray("station").getJSONObject(0).getString("name"),
                    station.getJSONArray("station").getJSONObject(0).getString("label"),
                    station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("region"),
                    station.getString("seasonalParameter"),
                    Parameters(
                        temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                            ), Pollutant(
                                "pm25",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                "µg/m³",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                            )
                        )
                    ),
                    forecast
                )

                val editor = sharedPreferences.edit()

                editor.putString("curLoc",stationObject.jsonString())

                editor.apply()
                val progressBar: ProgressBar = activity.findViewById(R.id.dataProgressBar)
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    progressBar.setProgress(1,true)
                else
                    progressBar.progress = 1
                if ( position < stationList.size){
                    GetFavoritesData(
                        activity,
                        JSONObject(stationList[position]).getJSONObject("station").getString("id"),
                        sharedPreferences,
                        stationList,
                        position + 1
                    ).execute()
                }else{
                    val intent = Intent(activity, MainActivity::class.java)
                    activity.startActivity(intent)
                }
            }catch (e: Exception){
                Snackbar.make(
                    activity.findViewById(R.id.dataProgressBar),
                    "" + e.localizedMessage,
                    Snackbar.LENGTH_INDEFINITE
                ).show()
                //val intent = Intent(activity, MainActivity::class.java)
                //activity.startActivity(intent)
            }

        }
    }

    class GetFavoritesData(activity: WelcomeActivity, station: String?, preferences: SharedPreferences, stationList: ArrayList<String>, position: Int): AsyncTask<Void, Void, String>(){
        var station = station
        var sharedPreferences = preferences
        var stationList = stationList
        var position = position
        var activity = activity
        override fun doInBackground(vararg params: Void?): String? {
            if (station=="")
                station="slc"

            val url = URL("https://air.utah.gov/phoneApp.php?p=station&id=$station")


            val httpClient = url.openConnection() as HttpsURLConnection
            try  {
                if((httpClient.responseCode == HttpsURLConnection.HTTP_OK)){
                try {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                    val stringBuilder = StringBuilder()
                    bufferedReader.forEachLine { stringBuilder.append(it) }
                    return stringBuilder.toString()
                }catch (e: Exception){
                    Snackbar.make(
                        activity.findViewById(R.id.dataProgressBar),
                        "Unable to connect to network",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }finally {
                    httpClient.disconnect()
                }
                }
            }catch (ex: Exception){
                Snackbar.make(
                    activity.findViewById(R.id.dataProgressBar),
                    "Unable to connect to network",
                    Snackbar.LENGTH_INDEFINITE
                ).show()
            }finally {
                httpClient.disconnect()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try{
                val station = JSONObject(result)

                var stationObject: Station

                var forecast = Array(station.getJSONArray("forecast").length()){

                        i ->
                    Forecast(
                        station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                        station.getJSONArray("forecast").getJSONObject(i).getString("message")
                    )

                }

                var temper = try {
                    station.getJSONObject("parameters").getInt("temperature")
                }catch (e: JSONException){
                    0
                }
                var windSpeed = try{
                    station.getJSONObject("parameters").getDouble("wind_speed")
                }catch (e: JSONException){
                    0.0
                }

                stationObject = Station(
                    station.getJSONArray("station").getJSONObject(0).getString("id"),
                    station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                    station.getJSONArray("station").getJSONObject(0).getString("name"),
                    station.getJSONArray("station").getJSONObject(0).getString("label"),
                    station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                    station.getJSONArray("station").getJSONObject(0).getString("region"),
                    station.getString("seasonalParameter"),
                    Parameters(
                        temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                        arrayOf(
                            Pollutant(
                                "ozone",
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                            ), Pollutant(
                                "pm25",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                                "µg/m³",
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                                station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                            )
                        )
                    ),
                    forecast
                )



                val editor = sharedPreferences.edit()

                editor.putString(stationObject.id,stationObject.jsonString())

                if (sharedPreferences.getString("curLoc","")!!.isBlank()){
                   editor.putString("curLoc",stationObject.jsonString())
                }

                editor.apply()

                val progressBar: ProgressBar = activity.findViewById(R.id.dataProgressBar)
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    progressBar.setProgress(progressBar.progress+1,true)
                else
                    progressBar.progress += 1


                if ( position < stationList.size){
                    GetFavoritesData(
                        activity,
                        JSONObject(stationList[position]).getJSONObject("station").getString("id"),
                        sharedPreferences,
                        stationList,
                        position + 1
                    ).execute()
                }else{
                    val intent = Intent(activity, MainActivity::class.java)
                    activity.startActivity(intent)
                }
            }catch (e: Exception){
                Snackbar.make(
                    activity.findViewById(R.id.dataProgressBar),
                    "Unable to connect to network",
                    Snackbar.LENGTH_INDEFINITE
                ).show()
                //val intent = Intent(activity, MainActivity::class.java)
                //activity.startActivity(intent)
            }
        }
    }

}

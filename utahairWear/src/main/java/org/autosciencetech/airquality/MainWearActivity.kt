package org.autosciencetech.airquality

import android.Manifest
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import com.google.android.gms.wearable.*
import com.google.android.gms.wearable.DataClient.OnDataChangedListener
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.lang.Exception
import java.lang.StringBuilder
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.net.ssl.HttpsURLConnection

class MainWearActivity : WearableActivity(), OnDataChangedListener{
    override fun onDataChanged(dataEvents: DataEventBuffer) =
        dataEvents.forEach{ event -> if (event.type == DataEvent.TYPE_CHANGED){
            event.dataItem.also { dataItem -> if (dataItem.uri.path!!.compareTo("/station") == 0){
                DataMapItem.fromDataItem(dataItem).dataMap.apply {
                    updateStation(getString("org.autosciencetech.utahair.station"))
                }
            }
            }
        }
        else if (event.type == DataEvent.TYPE_DELETED){

        }else{

        }
    }

    private lateinit var datetimeTextView: TextView
    private lateinit var locationTextView: TextView
    private lateinit var unitTextView: TextView
    private lateinit var valueTextView: TextView
    private lateinit var trendTextView: TextView
    private var fusedLocationClient: FusedLocationProviderClient? = null

    val PREFS_FILENAME = "org.autosciencetech.utahair"
    lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_wear)
        datetimeTextView = findViewById(R.id.datetimeTextView)
        locationTextView = findViewById(R.id.locationTextView)
        unitTextView = findViewById(R.id.unitTextView)
        valueTextView = findViewById(R.id.valueTextView)
        trendTextView = findViewById(R.id.trendTextView)
        prefs = this.getSharedPreferences(PREFS_FILENAME, 0)
        //if able get gps location for first run
        if (prefs.getString("curLoc","").orEmpty() == "" && checkPermissions()){
            var latitude: Double
            var longitude: Double
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)
            fusedLocationClient!!.lastLocation.addOnCompleteListener { task -> if (task.isSuccessful && task.result != null){
                val lastLocation = task.result
                latitude = lastLocation!!.latitude
                longitude = lastLocation.longitude
                if (isConnectedToNetwork()){
                    GetLocationData(
                        this,
                        latitude,
                        longitude,
                        prefs
                    ).execute()
                }
            }
            }
            //if no gps and first run get hawthorne station instead if internet is available
        }else if (prefs.getString("curLoc","").orEmpty() == "" && isConnectedToNetwork()){
            GetLocationData(
                this,
                -111.8717,
                40.7335,
                prefs
            ).execute()
        }else{
            //if not first run or no internet is available try to load previous data if available
            updateStation(prefs.getString("curLoc","").orEmpty())
        }



        // Enables Always-on
        setAmbientEnabled()
    }

    override fun onResume() {
        super.onResume()
        Wearable.getDataClient(applicationContext).addListener(this)
    }

    override fun onPause() {
        super.onPause()
        Wearable.getDataClient(applicationContext).removeListener(this)
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun updateStation(stationString: String?){
        if (stationString != null) {
            if (stationString.any()){
                val editor = prefs.edit()
                editor.putString("curLoc",stationString)
                editor.apply()
                val stationJson = JSONObject(stationString)
                locationTextView.text = stationJson.getJSONObject("station").getString("name")


                val pollutantType = prefs.getString("pollutant", stationJson.getString("seasonalParameter"))

                for (i in 0 until stationJson.getJSONObject("parameters").getJSONArray("pollutants").length()){
                    if(stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("type") == pollutantType){
                        valueTextView.text = stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("value")
                        unitTextView.text = stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("unit")

                        valueTextView.setTextColor(Color.parseColor("#"+stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("colorHex")))

                        when(stationJson.getJSONObject("parameters").getJSONArray("pollutants").getJSONObject(i).getString("arrow")){
                            "up_red" -> {
                                trendTextView.text = "\u2191"
                                trendTextView.setTextColor(Color.RED)
                            }
                            "down_green" -> {
                                trendTextView.text = "\u2193"
                                trendTextView.setTextColor(Color.GREEN)
                            }
                            else -> {
                                trendTextView.text = "\u2192"
                                trendTextView.setTextColor(Color.GRAY)
                            }
                        }
                    }
                }
            }else{
                locationTextView.text = "No Data Available"
                valueTextView.text = "N/A"
                unitTextView.text = ""
            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            datetimeTextView.text = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a"))
        else
            datetimeTextView.text = SimpleDateFormat("MM/dd/yyyy hh:mm a").format(Date())
    }
}

private class GetLocationData(context: Context, latitude: Double, longitude: Double, sharedPreferences: SharedPreferences): AsyncTask<Void, Void, String>(){
    val latitude = latitude
    val longitude = longitude
    val context = context

    val prefs = sharedPreferences
    override fun doInBackground(vararg params: Void?): String? {

        val url = URL("https://air.utah.gov/phoneApp.php?p=gps&lat=$latitude&long=$longitude")

        if (context.isConnectedToNetwork()){
            val httpClient = url.openConnection() as HttpsURLConnection
            try  {
                if((httpClient.responseCode == HttpsURLConnection.HTTP_OK)){
                    try {
                        val stream = BufferedInputStream(httpClient.inputStream)
                        val bufferedReader = BufferedReader(InputStreamReader(stream) as Reader?)
                        val stringBuilder = StringBuilder()
                        bufferedReader.forEachLine { stringBuilder.append(it) }
                        return stringBuilder.toString()
                    }catch (e: Exception){
                        e.printStackTrace()
                    }finally {
                        httpClient.disconnect()
                    }
                }
            }catch (ex: Exception){
                ex.printStackTrace()
            }finally {
                httpClient.disconnect()
            }
        }
        return null
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        try{
            val station = JSONObject(result)
            var stationObject: Station

            var forecast = Array(station.getJSONArray("forecast").length()){

                    i ->
                Forecast(
                    station.getJSONArray("forecast").getJSONObject(i).getString("date"),
                    station.getJSONArray("forecast").getJSONObject(i).getString("dayOfWeek"),
                    station.getJSONArray("forecast").getJSONObject(i).getString("severity"),
                    station.getJSONArray("forecast").getJSONObject(i).getString("action"),
                    station.getJSONArray("forecast").getJSONObject(i).getString("message")
                )

            }

            var temper = try {
                station.getJSONObject("parameters").getInt("temperature")
            }catch (e: JSONException){
                0
            }
            var windSpeed = try{
                station.getJSONObject("parameters").getDouble("wind_speed")
            }catch (e: JSONException){
                0.0
            }

            stationObject = Station(
                station.getJSONArray("station").getJSONObject(0).getString("id"),
                station.getJSONArray("station").getJSONObject(0).getString("websiteId"),
                station.getJSONArray("station").getJSONObject(0).getString("name"),
                station.getJSONArray("station").getJSONObject(0).getString("label"),
                station.getJSONArray("station").getJSONObject(0).getString("latitude"),
                station.getJSONArray("station").getJSONObject(0).getString("longitude"),
                station.getJSONArray("station").getJSONObject(0).getString("region"),
                station.getString("seasonalParameter"),
                Parameters(
                    temper, windSpeed, station.getJSONObject("parameters").getString("wind_dir"),
                    arrayOf(
                        Pollutant(
                            "ozone",
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("value"),
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("unit"),
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("color"),
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("colorHex"),
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("arrow"),
                            station.getJSONObject("parameters").getJSONObject("ozone").getString("range")
                        ), Pollutant(
                            "pm25",
                            station.getJSONObject("parameters").getJSONObject("pm25").getString("value"),
                            "µg/m³",
                            station.getJSONObject("parameters").getJSONObject("pm25").getString("color"),
                            station.getJSONObject("parameters").getJSONObject("pm25").getString("colorHex"),
                            station.getJSONObject("parameters").getJSONObject("pm25").getString("arrow"),
                            station.getJSONObject("parameters").getJSONObject("pm25").getString("range")
                        )
                    )
                ),
                forecast
            )

            val editor = prefs.edit()

            editor.putString("curLoc",stationObject.jsonString())

            editor.apply()

        }catch (e: Exception){

        }
    }
}